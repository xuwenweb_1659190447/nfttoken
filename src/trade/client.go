package trade

import (
	"gitcode.net/togolife/nfttoken/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

var txConf = &common.GlobalConf
var txLog = common.GetNftLog("trade")

func GetEthClient(chain string) (*ethclient.Client, common.NftError) {
	v, ok := txConf.SupportChains[chain]
	if !ok {
		txLog.LogE("Unsupported input chain [%v]", chain)
		return nil, common.NewError(505)
	}
	client, err := ethclient.Dial(v.NodePath)
	if err != nil {
		txLog.LogE("Get eth client failed! [%v]", err)
		return nil, common.NewError(500)
	}
	return client, common.NewError(200)
}
