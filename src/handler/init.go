package handler

import "gitcode.net/togolife/nfttoken/common"

var hdConf = &common.GlobalConf
var hdLog = common.GetNftLog("handler")

type signCheck interface {
	checkSign() bool
}
