package common

import (
	"fmt"
	"time"
)

type NftLog struct {
	logLevel int
	logMode  string
}

const (
	DEBUG = iota
	INFO
	WARN
	ERROR
)

func GetNftLog(mode string) NftLog {
	var log NftLog
	log.logLevel = GlobalConf.LogLevel
	log.logMode = mode
	return log
}

func (log NftLog) logPrint(head string, format string, args ...interface{}) {
	timeStr := fmt.Sprintf(time.Now().Format("2006-01-02 15:04:05.000000"))
	fmt.Printf(timeStr+" "+head+format+"\n", args...)
}

func (log NftLog) LogD(format string, args ...interface{}) {
	if log.logLevel > DEBUG {
		return
	}
	head := log.logMode + " DEBUG "
	log.logPrint(head, format, args...)
}

func (log NftLog) LogI(format string, args ...interface{}) {
	if log.logLevel > INFO {
		return
	}
	head := log.logMode + " INFO "
	log.logPrint(head, format, args...)
}

func (log NftLog) LogW(format string, args ...interface{}) {
	if log.logLevel > WARN {
		return
	}
	head := log.logMode + " WARN "
	log.logPrint(head, format, args...)
}

func (log NftLog) LogE(format string, args ...interface{}) {
	if log.logLevel > ERROR {
		return
	}
	head := log.logMode + " ERROR "
	log.logPrint(head, format, args...)
}
