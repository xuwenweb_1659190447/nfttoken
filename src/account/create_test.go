package account

import (
	"fmt"
	"testing"

	"gitcode.net/togolife/nfttoken/common"
)

func TestCreateAccount(t *testing.T) {
	common.LoadConfig("../conf/config.conf")
	address, err := CreateAccount()
	if err != nil {
		t.Fatalf("Create account test failed! %v", err)
	}
	fmt.Printf("Create account : %v\n", address)
}

func TestGetCurrentAccount(t *testing.T) {
	common.LoadConfig("../conf/config.conf")
	address := GetCurrentAccounts()
	if len(address) > 0 {
		for _, addr := range address {
			t.Logf("%v", addr)
		}
	}
}

func TestImportAccount(t *testing.T) {
	common.LoadConfig("../conf/config.conf")
	privateKey := "0xd334346c49a173bef25c9e976302fee0996f1fbf54efdee3db4578531b7aec67"
	err := ImportAccount(privateKey)
	if err != nil {
		t.Fatalf("%v", err)
	}
}
