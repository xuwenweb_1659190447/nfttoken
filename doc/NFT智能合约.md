# ERC721协议智能合约

## ERC721协议

`ERC-721`定义了NFT（非同质化代币）标准。所谓非同质化代币是区分同质化代币的，一般来说现实生活中的纸币都是同质化，任意两张等额纸币代表的意义是一样的，在以太坊上对应`ERC-20`协议。而非同质化的则表示任意两个代币都是不同的，每个代币都具有唯一性。所以，`ERC-721`通常用来表示独一无二、唯一的收藏品。

`ERC-721`定义了智能合约必须实现的最低接口，以允许管理、拥有和交易独特的代币。如下所示

```js
pragma solidity ^0.4.20;

/// @title ERC-721 Non-Fungible Token Standard
/// @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
///  Note: the ERC-165 identifier for this interface is 0x80ac58cd
interface ERC721 /* is ERC165 */ {
    /// @dev This emits when ownership of any NFT changes by any mechanism.
    ///  This event emits when NFTs are created (`from` == 0) and destroyed
    ///  (`to` == 0). Exception: during contract creation, any number of NFTs
    ///  may be created and assigned without emitting Transfer. At the time of
    ///  any transfer, the approved address for that NFT (if any) is reset to none.
    event Transfer(address indexed _from, address indexed _to, uint256 indexed _tokenId);

    /// @dev This emits when the approved address for an NFT is changed or
    ///  reaffirmed. The zero address indicates there is no approved address.
    ///  When a Transfer event emits, this also indicates that the approved
    ///  address for that NFT (if any) is reset to none.
    event Approval(address indexed _owner, address indexed _approved, uint256 indexed _tokenId);

    /// @dev This emits when an operator is enabled or disabled for an owner.
    ///  The operator can manage all NFTs of the owner.
    event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);

    /// @notice Count all NFTs assigned to an owner
    /// @dev NFTs assigned to the zero address are considered invalid, and this
    ///  function throws for queries about the zero address.
    /// @param _owner An address for whom to query the balance
    /// @return The number of NFTs owned by `_owner`, possibly zero
    function balanceOf(address _owner) external view returns (uint256);

    /// @notice Find the owner of an NFT
    /// @dev NFTs assigned to zero address are considered invalid, and queries
    ///  about them do throw.
    /// @param _tokenId The identifier for an NFT
    /// @return The address of the owner of the NFT
    function ownerOf(uint256 _tokenId) external view returns (address);

    /// @notice Transfers the ownership of an NFT from one address to another address
    /// @dev Throws unless `msg.sender` is the current owner, an authorized
    ///  operator, or the approved address for this NFT. Throws if `_from` is
    ///  not the current owner. Throws if `_to` is the zero address. Throws if
    ///  `_tokenId` is not a valid NFT. When transfer is complete, this function
    ///  checks if `_to` is a smart contract (code size > 0). If so, it calls
    ///  `onERC721Received` on `_to` and throws if the return value is not
    ///  `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`.
    /// @param _from The current owner of the NFT
    /// @param _to The new owner
    /// @param _tokenId The NFT to transfer
    /// @param data Additional data with no specified format, sent in call to `_to`
    function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes data) external payable;

    /// @notice Transfers the ownership of an NFT from one address to another address
    /// @dev This works identically to the other function with an extra data parameter,
    ///  except this function just sets data to ""
    /// @param _from The current owner of the NFT
    /// @param _to The new owner
    /// @param _tokenId The NFT to transfer
    function safeTransferFrom(address _from, address _to, uint256 _tokenId) external payable;

    /// @notice Transfer ownership of an NFT -- THE CALLER IS RESPONSIBLE
    ///  TO CONFIRM THAT `_to` IS CAPABLE OF RECEIVING NFTS OR ELSE
    ///  THEY MAY BE PERMANENTLY LOST
    /// @dev Throws unless `msg.sender` is the current owner, an authorized
    ///  operator, or the approved address for this NFT. Throws if `_from` is
    ///  not the current owner. Throws if `_to` is the zero address. Throws if
    ///  `_tokenId` is not a valid NFT.
    /// @param _from The current owner of the NFT
    /// @param _to The new owner
    /// @param _tokenId The NFT to transfer
    function transferFrom(address _from, address _to, uint256 _tokenId) external payable;

    /// @notice Set or reaffirm the approved address for an NFT
    /// @dev The zero address indicates there is no approved address.
    /// @dev Throws unless `msg.sender` is the current NFT owner, or an authorized
    ///  operator of the current owner.
    /// @param _approved The new approved NFT controller
    /// @param _tokenId The NFT to approve
    function approve(address _approved, uint256 _tokenId) external payable;

    /// @notice Enable or disable approval for a third party ("operator") to manage
    ///  all of `msg.sender`'s assets.
    /// @dev Emits the ApprovalForAll event. The contract MUST allow
    ///  multiple operators per owner.
    /// @param _operator Address to add to the set of authorized operators.
    /// @param _approved True if the operator is approved, false to revoke approval
    function setApprovalForAll(address _operator, bool _approved) external;

    /// @notice Get the approved address for a single NFT
    /// @dev Throws if `_tokenId` is not a valid NFT
    /// @param _tokenId The NFT to find the approved address for
    /// @return The approved address for this NFT, or the zero address if there is none
    function getApproved(uint256 _tokenId) external view returns (address);

    /// @notice Query if an address is an authorized operator for another address
    /// @param _owner The address that owns the NFTs
    /// @param _operator The address that acts on behalf of the owner
    /// @return True if `_operator` is an approved operator for `_owner`, false otherwise
    function isApprovedForAll(address _owner, address _operator) external view returns (bool);
}

interface ERC165 {
    /// @notice Query if a contract implements an interface
    /// @param interfaceID The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceID) external view returns (bool);
}

interface ERC721TokenReceiver {
    /// @notice Handle the receipt of an NFT
    /// @dev The ERC721 smart contract calls this function on the
    /// recipient after a `transfer`. This function MAY throw to revert and reject the transfer. Return
    /// of other than the magic value MUST result in the transaction being reverted.
    /// @notice The contract address is always the message sender.
    /// @param _operator The address which called `safeTransferFrom` function
    /// @param _from The address which previously owned the token
    /// @param _tokenId The NFT identifier which is being transferred
    /// @param _data Additional data with no specified format
    /// @return `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`
    /// unless throwing
    function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes _data) external returns(bytes4);
    }
```

---

## 实现一个ERC721智能合约

### openzeppelin

[openzeppelin](https://github.com/OpenZeppelin)是一个开源的智能合约框架，实现了大部分已支持的合约协议。可以直接引用其已实现的`erc721 mock`合约，也可在其定义的合约接口基础上进行二次开发。

---

### 自写合约分析

本项目中，我们参考已有`erc721`合约的编写方式，自写实现一个简单的智能合约。

合约文件目录结构如下所示：

    ```
    contract
    ├─ nft-token-erc721.sol  # 合约实现文件
    │
    ├─base
    │      erc721-enumerable.sol  # 支持枚举
    │      erc721-metadata.sol    # 支持元数据
    │      erc721-token-receiver.sol # 判断发送代币给合约
    │      erc721.sol # erc721协议定义
    │
    ├─ownership
    │      ownable.sol # 合约所有者
    │
    └─utils
            address-utils.sol # 地址工具
            erc165.sol # erc165协议定义
            supports-interface.sol # 支持接口定义
    ```

`erc721.sol`中定义了`ERC721`合约必须支持的接口，然后再编写`nft-token-erc721.sol`时就需要实现所有接口，同时我们还扩展了铸造(`mint`)和销毁(`burn`)两个接口，基本满足合约功能。

`nft-token-erc721.sol`实现简单解析：

- 定义内部数据结构，存储合约数据
    ```
    string internal nftName; // 合约名称
    string internal nftSymbol; // 合约符号
    uint256 internal tokenNums; // 合约已存在token总数
    mapping (uint256 => address) internal idToOwner; // 合约每个token与所属者信息
    mapping (uint256 => address) internal idToApproval; // 合约每个token与授权者信息
    mapping (address => mapping (address => bool)) internal ownerToOperators; // 合约内每个地址与其授权地址关系
    mapping (uint256 => string) internal idToUri; // 合约内每个token与uri信息
    mapping (address => uint256) private ownerToNFTokenCount; // 合约内某个地址持有token数量信息
    ```

- 定义内部控制函数

    - canOperate 调用者为token所有者或调用者已被token所有者授权
    - canTransfer 调用者为token所有者或调用者已被授权可操作该token或调用者已被token所有者授权
    - validNFToken 判断tokenid是否存在

---

## 智能合约编译

智能合约编译有多种方式，笔者觉得最简单的就是直接在[Remix](https://remix.ethereum.org/)网页上编译。

---

## 智能合约部署

在`Remix`上编译智能合约后可直接继续进行发布操作，不过笔者连上钱包后，却总是部署不成功。

故笔者采用`web3`方式部署合约，如下所示`nodejs`代码，配置

```js
const path = require('path');
const fs = require('fs');
let Web3 = require('web3');
let web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545')) // 填写可用的节点访问路径

const deploy = async(abi, bytecode, name, symbol, privateKey) => {
    let abiStr = fs.readFileSync(abi).toString();
    let byteCodeStr = "0x" + fs.readFileSync(bytecode).toString();
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    console.log('Attempting to deploy from account', account);
    web3.eth.accounts.wallet.add(account);
    const contract = new web3.eth.Contract(JSON.parse(abiStr));
    const deployTx = contract.deploy({data: byteCodeStr, arguments: [name, symbol]});
    const gas = await deployTx.estimateGas();
    console.log(`deploy tx gas ${gas}`);
    const deployedContract = await deployTx
        .send({from: account.address, gas: await deployTx.estimateGas()})
        .once('transactionHash', txhash => {
            console.log(`mining deployment transaction is ${txhash}`);
        });
    console.log(`contract deployed at ${deployedContract.options.address}`);
}

deploy('./nft-token-erc721.abi', // abi文件路径
       './nft-token-erc721.bin', // bin文件路径
       'WeNft', // nft 名字
       'WeTestNft', // nft 符号
       '' // 合约部署者私钥
    );
```

其中需要拷贝`remix`上编译生成合约的abi和bytecode到本地，并保存到`nft-token-erc721.abi`，`nft-token-erc721.bin`文件。

---

## 生成golang库，提供访问合约接口

在编译生成abi文件后，通过`go-ethereum`库中提供的abigen工具生成golang文件，该文件中提供了访问智能合约的接口方法。

- abigen工具

    需运行以下命令即可安装abigen工具。

    ```shell
    go get -u github.com/ethereum/go-ethereum
    cd $GOPATH/src/github.com/ethereum/go-ethereum/
    make
    make devtools
    ```

- 生成golang文件

    执行： `abigen --abi=nft-token-erc721.abi --pkg=contract --out=Token.go`

    `src/contract/Token.go`文件即为通过abigen命令生成的go文件。

